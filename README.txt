This Splunk add-on collects events from the Trello board API.

Trello API reference: http://api.trello.com/
Download Splunk Enterprise to get started for free: http://splunk.com/
